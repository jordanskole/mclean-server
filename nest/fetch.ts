import { APIGatewayProxyHandler } from 'aws-lambda';
import { nestClient, gqlClient, particleClient } from '../utilities/axios'
import { createMeasurement } from '../utilities/mutations'

const {
  NEST_URL,
  NEST_HALLWAY_ID,
} = process.env;

const particleString = (temp, humidity) => ({'name': 'NEST', 'data': `{ "temp": "${temp}", "humidity": "${humidity}" }`});

export const FetchNestReadings: APIGatewayProxyHandler = async (event) => {
  try {
    const { data: { devices: { thermostats } } } = await nestClient.get(NEST_URL);
    const sensorReading = thermostats[NEST_HALLWAY_ID];
    const variables = {
      "tempValue": parseFloat(sensorReading.ambient_temperature_f),
      "humidityValue": parseFloat(sensorReading.humidity),
      "sensedOn": new Date().toISOString(),
      "sensorId": NEST_HALLWAY_ID
    }

    const graphRes: any = await gqlClient.request(createMeasurement, variables);
    const particleResponse = await particleClient.post(`/v1/devices/events`, particleString(graphRes.temp.value, graphRes.humidity.value));
  } catch(e) {
    console.log(e);
  }

  return {
    statusCode: 200,
    body: JSON.stringify({
      message: 'Go Serverless Webpack (Typescript) v1.0! Your function executed successfully!',
      input: event,
    }),
  };
}