export const createMeasurement = `
mutation CreateMeasurement(
    $tempValue: Float,
    $humidityValue: Float,
    $sensorId: String,
    $sensedOn: DateTime
  ) {
  temp: createMeasurement(data: {
    measurementType: TEMP
    value: $tempValue
    status: PUBLISHED
    sensedOn: $sensedOn
    sensor: {
      connect: {
        sensorId: $sensorId
      }
    }
  }) {
    value
  }
  humidity: createMeasurement(data: {
    measurementType: HUMIDITY
    value: $humidityValue
    status: PUBLISHED
    sensedOn: $sensedOn
    sensor: {
      connect: {
        sensorId: $sensorId
      }
    }
  }) {
    value
  }
}
`