import axios from 'axios'
import { GraphQLClient } from 'graphql-request'

const {
  GRAPHCMS_URL,
  GRAPHCMS_AUTH_TOKEN,
  NEST_AUTH_TOKEN,
  PARTICLE_AUTH_TOKEN,
  PARTICLE_URL
} = process.env;

//
export const nestClient = axios.create({
  'headers': {
    'Authorization': `Bearer ${NEST_AUTH_TOKEN}`,
    'Content-Type': 'application/json'
  }
})

export const gqlClient = new GraphQLClient(GRAPHCMS_URL, {
  'headers': {
    'Authorization': `Bearer ${GRAPHCMS_AUTH_TOKEN}`,
  }
})

export const particleClient = axios.create({
  'baseURL': PARTICLE_URL,
  'headers': {
    'Authorization': `Bearer ${PARTICLE_AUTH_TOKEN}`,
    // 'Content-Type': 'application/x-www-form-urlencoded'
  }
});